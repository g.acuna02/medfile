import DashboardLayout from '@/views/Layout/DashboardLayout.vue';
const routes = [
  {
    path: '/',
    redirect: 'perfil',
    component: DashboardLayout,
    children: [
      {
        path: '/perfil',
        name: 'perfil',
        component: () => import(/* webpackChunkName: "demo" */ '../views/Pages/UserProfile.vue')
      },
      {
        path: '/examenes',
        name: 'examenes',
        component: () => import(/* webpackChunkName: "demo" */ '../views/Pages/Exam.vue')
      },
      {
        path: '/prescripciones',
        name: 'prescripciones',
        component: () => import(/* webpackChunkName: "demo" */ '../views/Pages/Receipt.vue')
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "demo" */ '../views/Pages/Login.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import(/* webpackChunkName: "demo" */ '../views/Pages/Register.vue')
  },

];

export default routes;
