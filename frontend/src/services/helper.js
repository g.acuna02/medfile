export function handleResponse(response) {
    return response.text().then(res => {
        const data = res && JSON.parse(res);
        if (!response.ok) {
            if(response.status == 401){
                localStorage.clear();
                window.location.reload();
            }
            return Promise.reject(data.errors);
        }
        return data;
    });
}