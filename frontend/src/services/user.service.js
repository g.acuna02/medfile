import { handleResponse } from './helper';

const apiUrl = 'http://localhost:5000';

function getMe() {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    };

    return fetch(apiUrl + '/me', requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function editProfile(profile) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(profile)
    };

    return fetch(apiUrl + '/me/profile', requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function editRecord(record) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(record)
    };

    return fetch(apiUrl + '/me/record', requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function uploadExam(exam) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: exam
    };

    return fetch(apiUrl + '/exams', requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function getSpecializations() {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    };

    return fetch(apiUrl + '/specialities', requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function getExams() {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    };

    return fetch(apiUrl + '/exams', requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function deleteExam(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    };

    return fetch(apiUrl + '/exams/'+id, requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function changeVisibility_exam(id) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    };

    return fetch(apiUrl + '/exams/'+id+'/visibility', requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function uploadReceipt(receipt) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: receipt
    };

    return fetch(apiUrl + '/receipts', requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function getReceipts() {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    };

    return fetch(apiUrl + '/receipts', requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function deleteReceipt(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    };

    return fetch(apiUrl + '/receipts/'+id, requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function changeVisibility_receipt(id) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    };

    return fetch(apiUrl + '/receipts/'+id+'/visibility', requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function generateCredentials() {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    };

    return fetch(apiUrl + '/generate', requestOptions)
        .then(handleResponse)
        .then(res => { return res; });
}

function getFile(file) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    };

    return fetch(apiUrl + '/files/'+file, requestOptions)
        .then(res => { return res; });
}

export const userService = {
    getMe,
    editProfile,
    editRecord,
    uploadExam,
    getSpecializations,
    getExams,
    deleteExam,
    changeVisibility_exam,
    uploadReceipt,
    getReceipts,
    deleteReceipt,
    changeVisibility_receipt,
    generateCredentials,
    getFile
};