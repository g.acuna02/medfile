require('dotenv').config()

const config = require('./config.js');
const express = require('express');
const methodOverride = require('method-override');
const mongoose = require('mongoose');
const cors = require('cors');
const fs = require('fs');
const multer = require('multer');

if (!fs.existsSync(__dirname + "/files")) {
    fs.mkdirSync(__dirname + "/files");
}

const app = express();
app.use(methodOverride());
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(cors());


function connectDB() {
    //Conectar con base de datos mongo
    return mongoose.connect(`mongodb://${config.DB_HOST}:${config.DB_PORT}/${config.DB_NAME}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => {
        mongoose.set('useCreateIndex', true);
        return true;
    }).catch(async err => {
        console.log('Error al conectar a la Base de datos. ' + err);
        return false;
    });
}

function loadModels() {
    //Cargar los models
    require('./models/user.model.js');
    require('./models/credentials.model.js');
    require('./models/speciality.model.js');
    require('./models/exam.model.js');
    require('./models/receipt.model.js');
}

function initRoutes() {
    const middlewares = require('./middlewares');

    //Inicializar las rutas
    const authRoutes = require('./routes/auth.route.js');
    const protectedRoutes = require('./routes/protected.route.js');
    app.use('/auth', authRoutes);
    app.use('/', middlewares.isAuth, protectedRoutes);


    //Error upload
    app.use((error, req, res, next) => {
        if (error instanceof multer.MulterError) {
            if (error.code == "LIMIT_FILE_SIZE")
                return res.status(500).send({
                    errors: ["Archivo muy pesado (Máximo 5MB)"]
                });
        }
        return res.status(500).send({
            errors: [error.message]
        });
    })
}

async function initServer() {
    const isDBConnected = await connectDB();
    if (isDBConnected) {
        loadModels();
        initRoutes();

        app.listen(config.PORT, () => {
            console.log(`HTTP Server running on port ${config.PORT}`)
        })
    }
}

initServer();

