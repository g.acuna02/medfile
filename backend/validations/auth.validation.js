var validator = require('validator');
var moment = require('moment');

module.exports = {
    //LOGIN
    checkLogin: function (req) {
        let error = [];
        if (!req.body.email || validator.isEmpty(req.body.email + "")) error.push("Falta email");
        if (!req.body.password || validator.isEmpty(req.body.password + "")) error.push("Falta password");

        return error;
    },

    //REGISTER
    checkRegister: function (req) {
        let error = [];
        //EMAIL
        if (!req.body.email || validator.isEmpty(req.body.email + "")) error.push("Falta email");
        else if (!validator.isEmail(req.body.email + "")) {
            error.push("Hubo un problema validando el email");
        }
        
        if (!req.body.nombre || validator.isEmpty(req.body.nombre + "")) error.push("Falta nombre");
        if (!req.body.apellido || validator.isEmpty(req.body.apellido + "")) error.push("Falta apellido");
        if (!req.body.password || validator.isEmpty(req.body.password + "")) error.push("Falta password");
        else if (req.body.password.length < 6) error.push("La contraseña debe ser de un mínimo de 6 caracteres");

        if (!req.body.fechaNac || validator.isEmpty(req.body.fechaNac + "")) error.push("Falta fecha de nacimiento");
        else {
            var date = new Date(req.body.fechaNac);
            if (!moment(date, 'YYYY-MM-DD', true).isValid()) error.push("La fecha de nacimiento debe ser válida (YYYY-MM-DD)");
        }

        return error;
    }
}