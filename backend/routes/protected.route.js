/**
 * Rutas protegidas por JWT
 */

const express = require("express"),
    protectedRouter = express.Router(),
    middleware = require('../middlewares'),
    uploadFile = require('../utils/uploadFile.js'),
    //Controllers
    userController = require("../controllers/user.controller.js"),
    specialityController = require("../controllers/speciality.controller.js");

protectedRouter
    .get("/me", userController.getMe)
    .put("/me/profile", middleware.isNotTemporal, userController.editProfile)
    .put("/me/record", middleware.isNotTemporal, userController.editRecord)
    .get("/generate", middleware.isNotTemporal, userController.generateCredentials)

    //Exams
    .post("/exams", middleware.isNotTemporal, uploadFile.upload, userController.uploadExam)
    .put("/exams/:idExam/visibility", middleware.isNotTemporal, userController.visibilityExam)
    .get("/exams", userController.getExams)
    .delete("/exams/:idExam", middleware.isNotTemporal, userController.deleteExam)

    //Receipt
    .post("/receipts", middleware.isNotTemporal, uploadFile.upload, userController.uploadReceipt)
    .put("/receipts/:idReceipt/visibility", middleware.isNotTemporal, userController.visibilityPresc)
    .get("/receipts", userController.getReceipts)
    .delete("/receipts/:idReceipt", middleware.isNotTemporal, userController.deleteReceipt)

    //Speciality
    .post("/specialities", middleware.isNotTemporal, specialityController.create)
    .get("/specialities", middleware.isNotTemporal, specialityController.getAll)
    
    //File
    .get("/files/:file", uploadFile.getFile);

module.exports = protectedRouter;