//User model
var mongoose = require("mongoose");

const usuarioSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        max: 50,
        min: 2
    },
    apellido: {
        type: String,
        required: true,
        max: 50,
        min: 2
    },
    genero: {
        type: String,
        required: false,
        uppercase: true
    },
    email: {
        type: String,
        lowercase: true,
        index: true,
        unique: true,
        required: true
    },
    fechaNac: {
        type: Date,
        required: true
    },
    prevision: {
        type: String,
        uppercase: true,
        required: false
    },
    tipoSangre: {
        type: String,
        uppercase: true,
        required: false
    },
    antecedentes: {
        alergias: {
            type: String
        },
        cirugias: {
            type: String
        },
        medicamentos: {
            type: String
        },
        diabetes: {
            type: Boolean,
            default: false
        },
        tiroides: {
            type: Boolean,
            default: false
        },
        hipertension: {
            type: Boolean,
            default: false
        },
        cardiopatias: {
            type: Boolean,
            default: false
        }
    }
});

mongoose.model('Usuario', usuarioSchema);