var mongoose = require("mongoose");

const receiptSchema = new mongoose.Schema({
    visibilidad: {
        type: Boolean,
        default: true
    },
    usuarioId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Usuario",
        required: true
    },
    observacion: {
        type: String,
        required: false
    },
    file: {
        type: String,
        required: true
    },
});

mongoose.model('Prescripcion', receiptSchema);