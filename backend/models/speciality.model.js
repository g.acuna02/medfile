var mongoose = require("mongoose");

const specialitySchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        unique: true
    }
});

mongoose.model('Especialidad', specialitySchema);