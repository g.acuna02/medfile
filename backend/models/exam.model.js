var mongoose = require("mongoose");

const examSchema = new mongoose.Schema({
    fecha: {
        type: String,
        required: true
    },
    visibilidad: {
        type: Boolean,
        default: true
    },
    usuarioId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Usuario",
        required: true
    },
    especialidadId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Especialidad",
        required: true
    },
    file: {
        type: String,
        required: true
    },
});

mongoose.model('Examen', examSchema);