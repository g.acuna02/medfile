var mongoose = require('mongoose'),
    Speciality = mongoose.model('Especialidad');

function getAll(req, res) {
    Speciality.find().sort('nombre').exec(function(err, result){
        if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});

        return res.status(200).send(result);
    })
}

function create(req, res){
    const speciality = new Speciality({
        nombre: req.body.nombre
    });

    speciality.save(function(err, saved){
        if (err) return res.status(500).send({ errors: ["Ocurrió un error inesperado"]});
        
        return res.status(201).send(saved);
    })
}

module.exports = {
    getAll,
    create
}